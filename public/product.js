var btnLogout = document.getElementById('logout');
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span id='member-btn' class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
             //點擊帳號會跳到會員資料區
var memberbtn=document.getElementById('member-btn');
memberbtn.addEventListener('click', function () {
 document.location.href = "member.html";
});


            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (user) {
                    alert("Logout success");
                    document.location.href = "index.html";
                    console.log(user);
                }).catch(function (error) {
                    // Handle Errors here.
                    alert("Logout fail");
                    console.log(error);

                });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='auth.html'>Login/Sign up</a>";
        }
    });
};
window.onload = function () {
    init();
};