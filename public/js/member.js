var btnLogout = document.getElementById('logout-btn');

window.onload = function init() {    //logout function
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='member-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            //點擊帳號會跳到會員資料區
var memberbtn=document.getElementById('member-btn');
memberbtn.addEventListener('click', function () {
 document.location.href = "member.html";
});
var changebtn=document.getElementById('change_btn');
changebtn.addEventListener('click', function () {
 document.location.href = "member_form.html";
});

            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (user) {
                    alert("Logout success");
                    document.location.href = "index.html";
                    console.log(user);
                }).catch(function (error) {
                    // Handle Errors here.
                    alert("Logout fail");
                    console.log(error);

                });
            });
            

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='auth.html'>Login/Sign up</a>";
        }
    }); 


//show member data
var search_btn = document.getElementById('search_btn');
var nameInfo=document.getElementById('name_bar');
var ageInfo=document.getElementById('age_bar');
var birthdayInfo=document.getElementById('birthday_bar');
var phoneInfo=document.getElementById('phone_bar');
var emailInfo=document.getElementById('email_bar');
       
var id;
search_btn.addEventListener('click', function () {
id = firebase.auth().currentUser;
firebase.database().ref('users/' + id.uid).once('value').then(function(snapshot) {
    console.log(id);
    var userInfoname = "姓名： "+snapshot.val().user_name
    var userInfoage="年齡： "+snapshot.val().user_age;
    var userInfobirthday="生日： "+snapshot.val().user_birthday;
    var userInfophone="電話號碼： "+snapshot.val().user_phone;
    var userInfoemail="信箱： "+snapshot.val().user_email;
    console.log(userInfoname);
    nameInfo.innerHTML = userInfoname;
    ageInfo.innerHTML=userInfoage;
    birthdayInfo.innerHTML=userInfobirthday;
    phoneInfo.innerHTML=userInfophone;
    emailInfo.innerHTML=userInfoemail;

  });
});


};




