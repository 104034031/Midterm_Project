var total=0;
window.onload = function init() {    //logout function
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='member-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            //點擊帳號會跳到會員資料區
var memberbtn=document.getElementById('member-btn');
memberbtn.addEventListener('click', function () {
 document.location.href = "member.html";
});

            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (user) {
                    alert("Logout success");
                    document.location.href = "index.html";
                    console.log(user);
                }).catch(function (error) {
                    // Handle Errors here.
                    alert("Logout fail");
                    console.log(error);

                });
            });
            

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='auth.html'>Login/Sign up</a>";
        }
    }); 


//show member data
var show_btn = document.getElementById('show_btn');
var product1Info=document.getElementById('product1_bar');
var product2Info=document.getElementById('product2_bar');
var product3Info=document.getElementById('product3_bar');
var product4Info=document.getElementById('product4_bar');
var product5Info=document.getElementById('product5_bar'); 
var product6Info=document.getElementById('product6_bar');
var product7Info=document.getElementById('product7_bar');
var product8Info=document.getElementById('product8_bar');
var product9Info=document.getElementById('product9_bar');   
var totalInfo=document.getElementById('total_bar');

var id;
//var p1;
//var p2;
//var p3;

//var total_price;
show_btn.addEventListener('click', function () {
id = firebase.auth().currentUser;
//1
firebase.database().ref('products/' + id.uid+'/product1/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product1_number)*800;
    var userInfop1 = "We Bare Bears 三隻呆熊娃娃 ： "+snapshot.val().product1_number+"隻";
    console.log(userInfop1);
    console.log(total);
    
    product1Info.innerHTML = userInfop1;

  });
//2
firebase.database().ref('products/' + id.uid+'/product2/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product2_number)*300;
    var userInfop2 = "We Bare Bears 皮革手機殼 ： "+snapshot.val().product2_number+"個";
    console.log(userInfop2);
    console.log(total);
    product2Info.innerHTML = userInfop2;

  });

//3
firebase.database().ref('products/' + id.uid+'/product3/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product3_number)*50;
    var userInfop3 = "We Bare Bears 百搭款中長襪 ： "+snapshot.val().product3_number+"雙";
    console.log(userInfop3);
    console.log(total);
    product3Info.innerHTML = userInfop3;
    
   
  });
//4
  firebase.database().ref('products/' + id.uid+'/product4/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product4_number)*600;
    var userInfop4 = "We bare bears 大抱枕 ： "+snapshot.val().product4_number+"個";
    console.log(userInfop4);
    console.log(total);
    product4Info.innerHTML = userInfop4;

  });
//5
  firebase.database().ref('products/' + id.uid+'/product5/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product5_number)*30;
    var userInfop5 = "We bare bears 胸針 ： "+snapshot.val().product5_number+"個";
    console.log(userInfop5);
    console.log(total);
    product5Info.innerHTML = userInfop5;

  });
//6
  firebase.database().ref('products/' + id.uid+'/product6/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product6_number)*700;
    var userInfop6 = "We bare bears 後背包 ： "+snapshot.val().product6_number+"個";
    console.log(userInfop6);
    console.log(total);
    product6Info.innerHTML = userInfop6;

  });
//7
  firebase.database().ref('products/' + id.uid+'/product7/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product7_number)*150;
    var userInfop7 = "We bare bears 三隻呆熊杯子 ： "+snapshot.val().product7_number+"個";
    console.log(userInfop7);
    console.log(total);
    product7Info.innerHTML = userInfop7;

  });
//8
  firebase.database().ref('products/' + id.uid+'/product8/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product8_number)*100;
    var userInfop8 = "We bare bears 霧面手機殼 ： "+snapshot.val().product8_number+"個";
    console.log(userInfop8);
    console.log(total);
    product8Info.innerHTML = userInfop8;

  });
//9
  firebase.database().ref('products/' + id.uid+'/product9/').once('value').then(function(snapshot) {
    console.log(id);
    total=total+(snapshot.val().product9_number)*200;
    var userInfop9 = "We bare bears 絨毛公仔 ： "+snapshot.val().product9_number+"隻";
    console.log(userInfop9);
    console.log(total);
    product9Info.innerHTML = userInfop9;

    var total_price="總共消費 ： "+total+"元";   //總共價錢放在最後一個商品
    totalInfo.innerHTML=total_price;

  });




//btn_function 結束
});


};

