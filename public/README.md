# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [熊熊買起來--購物網站]
* Key functions (add/delete)
    1. [Product page]
    2. [shopping pipeline]
    3. [user dashboard]
* Other functions (add/delete)
    1. [introduce page]
    2. [member form page]
    3. [shopping cart page]
    4. [Facebook & Google share boutton]
    5. [auto clear shopping cart after leaving shopping page]
    6. [if user haven't login,the system will alert and guide you to login page]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

1. 首先這是一個專門為We Bare Bears的愛好者設計的一個購物網站，首頁設計成一個專門介紹主題的introduce page，
   這個首頁有許多精美的動畫和可愛的介紹，是設計用來吸引顧客入坑的首頁，每個網頁左上角都有一個網站logo點擊它便會回到首頁。
2. 想要進入購物系統的話必須先由右上角點選登入或者是點擊products和相關購物按鈕，這些都會引導使用者到登入頁面，
   登入有分兩種方式email或是Google，登入成功後會建議使用者先去填寫資本資料，這些資料都會存到我們的firebase，
   會員想查看自己的基本資料可點選項列的Account自己的email，然後便可看到完整資訊。
3. 點擊"product"接著進入購物系統，底下會列出目前網站的所有商品，若是有興趣購買的話可以點選查看更多，會引導到商品頁面，
   商品頁面會有詳細的介紹，下面若是有意購買的話，可以點選數量並按放入購物車，系統將會顯示是否有成功放入。
4. 除此之外，還特別加設FB和Google的"分享按鈕"，方便使用者po文。
5. 最後都點選完成後可以點擊右上方的cart或是在product page最下面的結帳按鈕進到購物車頁面，這裡將會顯示出今天會員所點選的商品細項，
   並結算總共價錢，點選"去結帳"就會完成本次購物了，系統會再回到首頁。

## Security Report (Optional)
我設計的realtime database主要存兩個東西，一個是會員資料，另一個是會員點選的商品種類和數量。
會員這邊的安全性設定是read和write都必須是$uid==auth.uid; 確定不是其他非規定登入方式。
比較特別的是products,這裡如果要把商品放入購物車的話，必須先有會員登入，為了防止駭客跳過登入或者使用非本會員帳號來到購物車這裡，
特別增設點選時會判斷本會員是否已在本系統建立基本資料，以防止一些機器人帳號，如下面這行 “auth!=null && root.child('/users/'+$uid).exists()“
且我在本身網頁設計時，就有設計若要進入購物系統時，必須先進行登入，查看購物車資訊也是，都是一些在安全上做的加強。